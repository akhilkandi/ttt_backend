const express = require('express');
const port = 8000;
const app = express();
const fs = require('fs');
const fetch = require('node-fetch');
/*app.get('/', (req, res)=> res.send('hello'));*/
app.get('/:id', function gettext(req, res){
  fetch('https://terriblytinytales.com/test.txt')
      .then((resp) => resp.text())
      .then(function(data){
        //res=res.text();
        console.log(data);
        fs.writeFile("temp.txt",data,(err)=>{
          if(err) console.log(err);
          console.log("success");
        });


        var N = req.params.id;
        console.log(N);
        fs.readFile('temp.txt', 'utf8', function (err, data) {
          if (err) throw err;

          var wordsArray = splitByWords(data);
          var wordsMap = createWordMap(wordsArray);
          var finalWordsArray = sortByCount(wordsMap,N);

          console.log(finalWordsArray);
          res.jsonp(finalWordsArray);
        });



      });
  //res.send('hello');
});

app.listen(port, ()=>console.log('listening '));



function splitByWords (text) {
  var wordsArray = text.split(/\s+/);
  return wordsArray;
}


function createWordMap (wordsArray) {

  var wordsMap = {};

  wordsArray.forEach(function (key) {
    if (wordsMap.hasOwnProperty(key)) {
      wordsMap[key]++;
    } else {
      wordsMap[key] = 1;
    }
  });

  return wordsMap;

}


function sortByCount (wordsMap,N) {
  var n =N;
  var WordsArray = [];
  var finalWordsArray =[];
  WordsArray = Object.keys(wordsMap).map(function(key) {
    return {
      name: key,
      total: wordsMap[key]
    };
  });

  WordsArray.sort(function(a, b) {
    return b.total - a.total;
  });

  for(index=0;index<n;index++){
    finalWordsArray[index]= WordsArray[index];
  }



  return finalWordsArray;

}
